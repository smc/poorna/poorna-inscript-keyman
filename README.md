# Poorna Malayalam Extended Inscript Keyboard

![Poorna Keyboard](https://gitlab.com/smc/poorna/poorna-inscript-keyman/-/raw/main/poorna_malayalam_extended_inscript/extras/poorna_malayalam_extended_inscript.svg)

**Poorna Malayalam Extended Inscript** keyboard is a open source [Keyman](https://keyman.com/) keyboard. It is a complete Malayalam unicode character set keyboard. The key mapping of this keyboard is an extended version of the Malayalam Inscript layout.    

* [Poorna Website](https://poorna.smc.org.in)
* [Keyman Keyboard Home](https://keyman.com/keyboards/poorna_malayalam_extended_inscript)
* [Keyman Keyboard Help](https://help.keyman.com/keyboard/poorna_malayalam_extended_inscript)
* [Keyman Web](https://keymanweb.com/#ml,Keyboard_poorna_malayalam_extended_inscript)

## How to build

**Windows :**
* Clone/download this repo and set it as your working directory.
* Open Windows Terminal/PowerShell and run the following commands to download the Keyman Developer command line tools.

	```````
	mkdir kmcomp
	cd kmcomp
	Invoke-WebRequest -Uri "https://keyman.com/go/download/kmcomp" -OutFile "kmcomp.zip"
	Expand-Archive -Path "kmcomp.zip" -DestinationPath .
	```````

* Without changing the current directory, run the following command to build.
	```````
	./kmcomp.exe ../poorna_malayalam_extended_inscript/poorna_malayalam_extended_inscript.kpj
	```````
* To clean the build, run the following command.
	```````
	./kmcomp.exe -c ../poorna_malayalam_extended_inscript/poorna_malayalam_extended_inscript.kpj
	```````
	
**Linux, macOS :**
* **[WINE](https://www.winehq.org/)** is required in order to run the Keyman Developer compiler which is currently a Windows-only executable.
* See [this article](https://help.keyman.com/knowledge-base/95) for Mac configuration information.
* Clone/download this repo and set it as your working directory.
* Open Linux Terminal and run the following commands to download the Keyman Developer command line tools.

	```````
	mkdir kmcomp
	cd kmcomp
	curl -L https://keyman.com/go/download/kmcomp -o kmcomp.zip
	unzip kmcomp.zip
	```````

* Without changing the current directory, run the following command to build.
	```````
	wine ./kmcomp.exe ../poorna_malayalam_extended_inscript/poorna_malayalam_extended_inscript.kpj
	```````
* To clean the build, run the following command.
	```````
	wine ./kmcomp.exe -c ../poorna_malayalam_extended_inscript/poorna_malayalam_extended_inscript.kpj
	```````
**Build folder :**
* Upon successful build, a build folder will be created at `/poorna_malayalam_extended_inscript/build`.
* **`.kmp`** file is the installable package file -- installable in all Keyman end user products.
* **`.js`** file is the keyboard compiled to Javascript for use with KeymanWeb.

## Copyright

See [LICENSE.md](poorna_malayalam_extended_inscript/LICENSE.md)